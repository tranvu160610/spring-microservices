package com.example.microservices;

import java.math.BigDecimal;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.microservices.currencyconversion.services.CurrencyExchangeServiceProxy;

@RestController
public class CurrencyConversionController {

	@Autowired
	Environment env;

	@Autowired
	CurrencyExchangeServiceProxy proxy;
	
	Logger logger = LoggerFactory.getLogger(CurrencyConversionController.class);

	/**
	 * Approach 1: Make REST calls using RestTemplate.
	 * 
	 * @param from
	 * @param to
	 * @param quantity
	 * @return
	 */
	@GetMapping("/currency-converter/from/{from}/to/{to}/{quantity}")
	public CurrencyConversionBean convert(@PathVariable("from") String from, @PathVariable("to") String to,
			@PathVariable("quantity") BigDecimal quantity) {

		RestTemplate restTemplate = new RestTemplate();
		HashMap<String, String> variables = new HashMap<>();
		variables.put("from", from);
		variables.put("to", to);

		ResponseEntity<CurrencyConversionBean> response = restTemplate.getForEntity(
				"http://localhost:8000/currency-exchange/from/{from}/to/{to}", CurrencyConversionBean.class, variables);

		final BigDecimal rate = response.getBody().getExchangeRate();
		final Long id = response.getBody().getId();
		
		logger.info("Response is {}", response);

		return new CurrencyConversionBean(id, from, to, rate, quantity, rate.multiply(quantity),
				response.getBody().getPort());
	}

	@GetMapping("/currency-converter-feign/from/{from}/to/{to}/{quantity}")
	public CurrencyConversionBean convertWithFeigh(@PathVariable("from") String from, @PathVariable("to") String to,
			@PathVariable("quantity") BigDecimal quantity) {

		CurrencyConversionBean response = proxy.getExchangeValue(from, to);

		final BigDecimal rate = response.getExchangeRate();
		final Long id = response.getId();

		return new CurrencyConversionBean(id, from, to, rate, quantity, rate.multiply(quantity), response.getPort());
	}

}
