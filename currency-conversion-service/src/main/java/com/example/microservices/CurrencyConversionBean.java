package com.example.microservices;

import java.math.BigDecimal;

public class CurrencyConversionBean {
	private Long id;
	private String from, to;
	private BigDecimal exchangeRate, quantity, total;
	private int port;
	
	public CurrencyConversionBean() {}
	
	public CurrencyConversionBean(Long id, String from, String to, BigDecimal rate, 
			BigDecimal quantity, BigDecimal total, int port) {
		this.id = id;
		this.from = from;
		this.to= to;
		this.exchangeRate = rate;
		this.quantity = quantity; 
		this.total = total;
		this.port = port;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
}
