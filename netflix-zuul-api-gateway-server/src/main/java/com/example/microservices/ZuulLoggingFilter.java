package com.example.microservices;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
public class ZuulLoggingFilter extends ZuulFilter {
	
	private Logger logger = LoggerFactory.getLogger(ZuulLoggingFilter.class);

	/**
	 * true = execute filter for all requests
	 */
	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		
		HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
		logger.info("request -> {} request uri -> {}", request, request.getRequestURI());
		
		return null;
	}

	/**
	 * Valid filters are:
	 * 
	 * pre - before request is executed
	 * post - after request is executed
	 * error - only when errors occurr
	 * 
	 */
	@Override
	public String filterType() {
		return "pre";
	}

	/**
	 * Default priority is zero.
	 */
	@Override
	public int filterOrder() {
		return 1;
	}

}
