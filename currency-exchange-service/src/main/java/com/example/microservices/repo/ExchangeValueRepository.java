package com.example.microservices.repo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.microservices.ExchangeValue;

@Repository
public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Long> {

	// JPA uses java reflection to implement the query auto-magically.
	public ExchangeValue findByFromAndTo(String from, String to);
	
}
