package com.example.microservices;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class ExchangeValue {

	@Column(name="currency_from")
	private String from;
	
	@Column(name="currency_to")
	private String to;
	
	@Id
	@GeneratedValue
	private Long id;
	
	private BigDecimal exchangeRate;
	
	@Transient
	private int port;
	
	protected ExchangeValue() {}
	
	public ExchangeValue(long id, String from, String to, BigDecimal rate) {
		this.id = id;
		this.from = from;
		this.to = to;
		this.exchangeRate = rate;
	}
	
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getFrom() {
		return from;
	}
	public String getTo() {
		return to;
	}
	public Long getId() {
		return id;
	}
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}
	
}
