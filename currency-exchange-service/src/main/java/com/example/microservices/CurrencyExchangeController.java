package com.example.microservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.microservices.repo.ExchangeValueRepository;

@RestController
public class CurrencyExchangeController {

	@Autowired
	Environment env;

	@Autowired
	ExchangeValueRepository repo;

	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public ExchangeValue getExchangeValue(@PathVariable("from") String from, @PathVariable("to") String to) {

		ExchangeValue exchangeValue = repo.findByFromAndTo(from, to);

		if (exchangeValue != null)
			exchangeValue.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		return exchangeValue;
	}

}
