package com.example.microservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitsConfigurationController {
	
	@Autowired
	Configuration config;

	@GetMapping("/limits")
	public LimitsConfiguration retrieveLimitsConifgurations() {
		return new LimitsConfiguration(config.getMaximum(), config.getMinimum());
	}
	
}
