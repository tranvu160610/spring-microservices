package com.example.microservices;

public class LimitsConfiguration {
	private int maximum;
	private int minimum;
	
	protected LimitsConfiguration() {
		
	}
	
	public LimitsConfiguration(int max, int min) {
		super();
		this.maximum = max;
		this.minimum = min;
	}
	
	public int getMaximum() {
		return maximum;
	}
	public int getMinimum() {
		return minimum;
	}
}
